You can use the logos for whatever you want.

## Favicon
![alt text](./resources/logos/favicon.ico "Favicon")

---
## SVG
![alt text](./resources/logos/datepoll.svg "SVG logo")

---
## JPG
![alt text](./resources/logos/datepoll.jpg "JPG")

---
## PNG
![alt text](./resources/logos/datepoll.png "PNG")