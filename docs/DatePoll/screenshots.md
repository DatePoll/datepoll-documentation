Here are some example screenshots of a running DatePoll application.

## Home / Start page
![alt text](../resources/screenshots/home.png "Screenshot of the home / start page")

## Calendar
![alt text](../resources/screenshots/calendar.png "Screenshot of the calendar")

## Events
![alt text](../resources/screenshots/events.png "Screenshot of the event view")

## Event info
![alt text](../resources/screenshots/event_info.png "Screenshot of the event info view")

## Event user management
![alt text](../resources/screenshots/event_user_management.png "Screenshot of the event user management view")

## Group and subgroup management
![alt text](../resources/screenshots/groups.png "Screenshot of the group and subgroup management view ")

## DatePoll management
![alt text](../resources/screenshots/datepoll_management.png "Screenshot of the DatePoll management view")