# Home
## Useful links
### Screenshots
Please visit [this page](./screenshots.md) if you want to see some screenshots.

### Installation
Please head to [this page](./installation.md) for a detailed installation process.

### Updating your existing DatePoll instance.
We try to make the update process as easy as possible. [Click me](./update.md) for more informations.

### Repositories
Where all the magic happens.

* [DatePoll-Frontend](https://gitlab.com/DatePoll/datepoll-frontend)
* [DatePoll-Backend](https://gitlab.com/DatePoll/datepoll-backend-php)
* [DatePoll-Android](https://gitlab.com/DatePoll/datepoll-android)
* [DatePoll-Dockerized](https://gitlab.com/DatePoll/datepoll-dockerized)
* [This documentation](https://gitlab.com/DatePoll/datepoll-documentation)

### API Documentation
[Here](./API/index.md) you will find everything you need to know as a developer about the REST API.

### Logo resources
[Click me](./logos.md) for more informations about our logo.
