# Welcome to DatePoll-Documentation

## Welcome
DatePoll is a passionate project of young developers. It's changing nearly every day, have fun!

## Useful links
### Project website
Just type [DatePoll.org](https://datepoll.org) and you will see magic.

### Subprojects
[DatePoll](./DatePoll/index.md)

### Logo resources
[Click me](./logos.md) for more informations about our logo.
